import java.util.ArrayList;
import java.util.List;

public class SpinScore {
   private List<Frame> scores = new ArrayList<>();
   private int bonus;
   private final int totalFrames = 10;


    public SpinScore() {

    }

    public void throwBall(List<Frame> spins) {
        this.scores = spins;
    }

    public int calculateBasicScore() {
        return scores.stream().limit(totalFrames).mapToInt(Frame::calculateTotalScoreOfOneFrame).sum();
    }

    private int calculateBonus() {
        this.scores.stream().limit(totalFrames).forEach(frame -> {
            if (frame.isSpare()) {
                calculateSpareBonus(frame);
            } else if (frame.isStrike()) {
                calculateStrikeBonus(frame);
            }
        });
        return bonus;
    }

    public int calculateTotalScore() {
        return this.calculateBasicScore() + this.calculateBonus();
    }

    private boolean isSuccessiveStrike(int index) {
        return this.scores.get(index).isStrike() && this.scores.get(index+1).isStrike();
    }

    private void calculateStrikeBonus(Frame frame) {
        int index = this.scores.indexOf(frame);
        Frame theNextFrame = this.scores.get(index + 1);
        if (!this.isSuccessiveStrike(index) || index == 9) {
            bonus += theNextFrame.calculateTotalScoreOfOneFrame();
        } else {
            Frame theFrameAfterNext = this.scores.get(index + 2);
            bonus += theNextFrame.calculateTotalScoreOfOneFrame() + theFrameAfterNext.getFirstThrowSpins();
        }
    }

    private void calculateSpareBonus(Frame frame) {
        int index = this.scores.indexOf(frame);
        Frame theNextFrame = this.scores.get(index + 1);
        bonus += theNextFrame.getFirstThrowSpins();
    }

}
