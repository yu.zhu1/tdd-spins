public class Frame {
    private int firstThrowSpins;
    private int secondThrowSpins;

    public Frame(int firstThrowSpins, int secondThrowSpins) {
        this.firstThrowSpins = firstThrowSpins;
        this.secondThrowSpins = secondThrowSpins;
    }

    int getFirstThrowSpins() {
        return firstThrowSpins;
    }

    int calculateTotalScoreOfOneFrame() {
        return this.firstThrowSpins + this.secondThrowSpins;
    }

    boolean isSpare() {
        return this.firstThrowSpins < 10 && this.calculateTotalScoreOfOneFrame() == 10;
    }

    boolean isStrike() {
        return this.firstThrowSpins == 10 && this.secondThrowSpins == 0;
    }


}
