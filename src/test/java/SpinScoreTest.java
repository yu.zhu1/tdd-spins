import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;

import static org.junit.Assert.assertEquals;

public class SpinScoreTest {
    private SpinScore spinScore;
    private ArrayList<Frame> frames = new ArrayList<>();


    @Before
    public void setUp() throws Exception {
        spinScore = new SpinScore();

    }

    @Test
    public void should_return_0_when_no_throw() {
        assertEquals(0, spinScore.calculateBasicScore());
    }


    @Test
    public void should_return_number_of_spins_given_1_throw() {
        spinScore.throwBall(Collections.singletonList(new Frame(1, 0)));
        assertEquals(1, spinScore.calculateBasicScore());
    }

    @Test
    public void should_return_number_of_spins_given_2_throws() {
        spinScore.throwBall(Collections.singletonList(new Frame(1, 2)));
        assertEquals(3, spinScore.calculateBasicScore());
    }

    @Test
    public void should_return_sum_of_10_frames_when_get_total_score_given_10_frames_without_strike_or_spare() {
        while(frames.size() < 10) {
            frames.add(new Frame(1,2));
        }
        spinScore.throwBall(frames);
        assertEquals(30, spinScore.calculateBasicScore());
    }

    @Test
    public void should_return_score_with_bonus_given_first_frame_spare() {
        frames.add(new Frame(4,6));
        while(frames.size() < 10) {
            frames.add(new Frame(1,2));
        }
        spinScore.throwBall(frames);
        assertEquals(38, spinScore.calculateTotalScore());
    }

    @Test
    public void should_return_score_with_bonus_given_10_frames_with_spare_without_strike() {
        frames.add(new Frame(4,6));
        frames.add(new Frame(5,5));
        frames.add(new Frame(3,7));
        while(frames.size() < 10) {
            frames.add(new Frame(1,2));
        }

        spinScore.throwBall(frames);
        assertEquals(60, spinScore.calculateTotalScore());
    }

    @Test
    public void should_plus_spare_bonus_of_last_frame() {
        while(frames.size() < 9) {
            frames.add(new Frame(1,2));
        }
        frames.add(new Frame(4,6));
        frames.add(new Frame(4,4));
        spinScore.throwBall(frames);
        assertEquals(41, spinScore.calculateTotalScore());
    }

    @Test
    public void should_plus_strike_bonus_when_only_first_frame_is_strike() {
        frames.add(new Frame(10,0));
        while(frames.size() < 10) {
            frames.add(new Frame(1,2));
        }
        spinScore.throwBall(frames);
        assertEquals(40, spinScore.calculateTotalScore());
    }

    @Test
    public void should_plus_strike_bonus_when_last_frame_is_not_strike() {
        frames.add(new Frame(10,0));
        frames.add(new Frame(5,5));
        while(frames.size() < 8) {
            frames.add(new Frame(1,2));
        }
        frames.add(new Frame(10,0));
        frames.add(new Frame(5,5));
        frames.add(new Frame(5,4));

        spinScore.throwBall(frames);
        assertEquals(84, spinScore.calculateTotalScore());
    }

    @Test
    public void should_plus_bonus_of_the_last_frame_when_the_last_one_is_strike() {
        frames.add(new Frame(10,0));
        frames.add(new Frame(5,5));
        while(frames.size() < 8) {
            frames.add(new Frame(1,2));
        }
        frames.add(new Frame(10,0));
        frames.add(new Frame(10,0));
        frames.add(new Frame(5,4));
        spinScore.throwBall(frames);
        assertEquals(93, spinScore.calculateTotalScore());
    }

    @Test
    public void should_get_300_when_meeting_a_perfect_game() {

        while(frames.size() < 10) {
            frames.add(new Frame(10,0));
        }
        frames.add(new Frame(10,10));

        spinScore.throwBall(frames);
        assertEquals(300, spinScore.calculateTotalScore());
    }



}
